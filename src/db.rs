use aws_sdk_dynamodb::model::AttributeValue;
use aws_sdk_dynamodb::Client;
use lambda_runtime::{run, service_fn, tracing, Error, LambdaEvent};
use serde::{Deserialize, Serialize};

// data to push to the dynamodb table
#[derive(Debug, Serialize, Deserialize)]
pub struct RollDie {
    pub roll_id: String,
    pub roll_value: i32,
}
// required for AWS
#[derive(Debug, Serialize)]
struct FailureResponse {
    pub body: String,
}

// implement Display for the Failure response so that we can then implement Error.
impl std::fmt::Display for FailureResponse {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.body)
    }
}

#[derive(Deserialize)]
struct Request {
    req_id: String,
    result: i32,
}

#[derive(Serialize)]
struct Response {
    req_id: String,
    result: i32,
    msg: String,
}


async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Extract some useful info from the request
    let req_id = event.payload.req_id;
    let result = event.payload.result;
    let config = aws_config::load_from_env().await;
    let client = Client::new(&config);

    let mut roll_die = RollDie {
        roll_id: String::new(),
        roll_value: i32::from(0),
    };
    // set value
    roll_die.roll_id = String::from(req_id.clone());
    roll_die.roll_value = i32::from(result.clone());

    let roll_die_id = AttributeValue::S(roll_die.roll_id.clone());
    let roll_die_value = AttributeValue::N(roll_die.roll_value.to_string());
    // add to dynamodb
    let _resp = client
        .put_item()
        .table_name("die_results")
        .item("roll_id", roll_die_id)
        .item("roll_value", roll_die_value)
        .send()
        .await
        .map_err(|_err| FailureResponse {
            body: _err.to_string(),
        });

    // Prepare the response
    let resp = Response {
        req_id: event.context.request_id,
        result: roll_die.roll_value,
        msg: "Inserted result into DynamoDB".to_string(),
    };

    // Return `Response` (it will be serialized to JSON automatically by the runtime)
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();

    run(service_fn(function_handler)).await
}
