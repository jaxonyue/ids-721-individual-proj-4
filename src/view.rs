use aws_sdk_dynamodb::model::AttributeValue;
use aws_sdk_dynamodb::Client;
use lambda_runtime::{run, service_fn, tracing, Error, LambdaEvent};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct Request {
    req_id: String,
    result: i32,
    msg: String,
}

#[derive(Serialize)]
struct Response {
    req_id: String,
    msg: String,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Extract some useful info from the request
    let _req_id = event.payload.req_id;
    let _msg = event.payload.msg;
    let config = aws_config::load_from_env().await;
    let client = Client::new(&config);

    let _resp = client.scan().table_name("die_results").send().await?;
    format!("Response from DynamoDB: {:?}", _resp);
    
    let mut die_sum:i32 = 0;
    if let Some(item) = _resp.items {
        for i in item {
            let value = i.get("roll_value").unwrap().as_n()
            .unwrap()
            .to_string()
            .parse::<i32>()
            .unwrap();
            die_sum += value;
        }
    }

    // Prepare the response
    let resp = Response {
        req_id: event.context.request_id,
        msg: format!("Sum of All Rolls: {}", die_sum),
    };

    // Return `Response` (it will be serialized to JSON automatically by the runtime)
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();

    run(service_fn(function_handler)).await
}
