use lambda_runtime::{run, service_fn, tracing, Error, LambdaEvent};
use rand::Rng;
use serde::{Deserialize, Serialize};

// roll a die using random number generator and return the result
fn roll_die(command: String) -> Result<i32, Box<dyn std::error::Error>> {
    format!("Rolling a die with command: {}", command);
    let result = rand::thread_rng().gen_range(1..=6);
    Ok(result)
}

#[derive(Deserialize)]
struct Request {
    command: String,
}


#[derive(Serialize)]
struct Response {
    req_id: String,
    result: i32,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let command = event.payload.command;
    let mut res: i32 = -1;

    let message = match roll_die(command.to_string()) {
        Ok(inference_result) => {
            format!("{}", inference_result);
            res = inference_result;
        }
        Err(_err) => {
            // who cares 
        }
    };
    // Prepare the response
    let resp = Response {
        req_id: event.context.request_id,
        result: res,
    };

    // Return `Response` (it will be serialized to JSON automatically by the runtime)
    Ok(resp)
}


#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();

    run(service_fn(function_handler)).await
}
