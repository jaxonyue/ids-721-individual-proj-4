# IDS 721 Individual Proj 4 - Rust AWS Lambda and Step Functions [![pipeline status](https://gitlab.com/jaxonyue/ids-721-individual-proj-4/badges/main/pipeline.svg)](https://gitlab.com/jaxonyue/ids-721-individual-proj-4/-/commits/main)

## Overview
* This repository includes the components for **Individual Project 4 - Rust AWS Lambda and Step Functions**

## Demo Video
https://youtu.be/lCZTl2pBWuA

## Goal
* Rust AWS Lambda Function
* Step Functions workflow coordinating Lambdas
* Orchestrate data processing pipeline

## Dice Roll Function
* The dice roll function is a simple Rust Lambda function that rolls a dice with every execution, returns the result (1-6) and sums the total of all rolls
* There are three step functions that orchestrate the dice roll function
  * Step 1: `roll` - Rolls a dice and returns the result
  * Step 2: `insert` - Inserts the result into a DynamoDB table
  * Step 3: `sum` - Sums and returns the total of all dice rolls

## Screenshot of AWS State Machine
![Screenshot_2024-04-18_at_7.22.34_PM](/uploads/ce3c3cb1ee63693a494cbf580f5ff516/Screenshot_2024-04-18_at_7.22.34_PM.png)

![Screenshot_2024-04-18_at_7.21.47_PM](/uploads/847be07133d6156df2228f7a9901f382/Screenshot_2024-04-18_at_7.21.47_PM.png)

![Screenshot_2024-04-18_at_7.21.37_PM](/uploads/45c832ec19fa5bad517db6d6b6e90da8/Screenshot_2024-04-18_at_7.21.37_PM.png)

## Key Steps
1. Make a new Cargo Lambda project by `cargo lambda new <proj name>`
2. Implement the dice roll function in `src/main.rs`
3. Test the function locally by `cargo lambda watch` and then `cargo lambda invoke --data-ascii "{ \"command\": \"roll\"}`
4. Implement the insert function in `src/db.rs` and the sum function in `src/view.rs`
5. Add the necessary dependencies in `Cargo.toml`
6. Add the bins information for the 3 step functions in `Cargo.toml`
7. Go to AWS Console and attach `AWSLambda_FullAccess` and `IAMFullAccess` policies to the Lambda execution role
8. Create a new DynamoDB table with the primary key as `req_id`
9. Store AWS credentials in a `.env` file
10. Export the AWS credentials in the terminal by `source .env`
11. Build the project by `cargo lambda build`
12. Deploy the project by `cargo lambda deploy`
13. Create a new Step Functions state machine and add the 3 step functions
14. Test the state machine by starting a new execution

## References
* https://www.youtube.com/watch?v=qoVAEflgcZ4
* https://www.cargo-lambda.info/guide/getting-started.html
* https://docs.aws.amazon.com/step-functions/latest/dg/tutorial-create-new-state-machine.html